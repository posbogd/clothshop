﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Shop", menuName = "ScriptableObjects/ShopItems", order = 2)]
public class ItemList : ScriptableObject
{
   public List<Item> shopItems;
}
