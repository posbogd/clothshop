﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InventoryUI : MonoBehaviour
{
    public static InventoryUI Instance;
    public GameObject InventoryItemPrefab;
    public Transform itemsContainer;
    private int _money;
    [SerializeField] Text moneyCount;
    public int Money { get { return _money; } set { _money = value; moneyCount.text = value.ToString();} }
    public void Start()
    {
        Money = 100;
        if (Instance == null)
        {
            Instance = this;
        }
        else Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }
    public void BuyItem(Item item)
    {
        if (Money > item.price)
        {
            Money -= item.price;
            AddToInventory(item);
        }
        else
            Debug.Log("Not enough money to buy the item!");
    }
    public void SellItem(Item item)
    {
        Money += (int)(item.price * 0.75f);
    }
    public void AddToInventory(Item item)
    {
        Instantiate(InventoryItemPrefab, itemsContainer).GetComponent<ItemUI>().Set(item);
    }

}
