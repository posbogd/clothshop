﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ItemUI : MonoBehaviour
{
    [SerializeField] Text Price;
    [SerializeField] Text Name;
    [SerializeField] Image Icon;
    public bool isInventory;
    public ItemType type;
    Item item;
    public void Set(Item item)
    {
        this.item = item;
        if (!isInventory)
            Price.text = item.price.ToString();
        else
        {
            Price.text = ((int)(item.price *.75f)).ToString();
        }
        Name.text = item.name;
        Icon.sprite = item.Icon;
        type = item.itemType;
    }
    public void BuyItem()
    {
        InventoryUI.Instance.BuyItem(item);
    }
    public void WearItem()
    {
        Debug.Log($"Wearing {item.name}");
    }
    public void SellItem()
    {
        InventoryUI.Instance.SellItem(item);
        Destroy(gameObject);
    }
}
