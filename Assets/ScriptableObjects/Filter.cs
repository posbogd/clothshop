﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Filter : MonoBehaviour
{
    [SerializeField] Transform Content;
    // Start is called before the first frame update
    public void SetFilter(int itemType)
    {
        foreach (Transform i in Content)
        {
            if (i.GetComponent<ItemUI>().type == (ItemType)itemType) i.gameObject.SetActive(true);
            else
                i.gameObject.SetActive(false);
        }
    }
    public void ClearFilter()
    {
        foreach (Transform i in Content)
        {
            i.gameObject.SetActive(true);
        }
    }
}
