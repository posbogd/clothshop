﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/Item", order = 1)]
public class Item : ScriptableObject
{
    public string Name;
    public Sprite Icon;
    public ItemType itemType;
    public int price;
}

public enum ItemType {Head, Chest, Pants, Shoulders}
