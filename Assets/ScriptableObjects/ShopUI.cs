﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopUI : MonoBehaviour
{
    public GameObject ItemShopPrefab;
    public Transform ItemsContainer;
    [SerializeField]
    ItemList itemsList;

    public void Start()
    {
        foreach (var item in itemsList.shopItems)
        {
            Instantiate(ItemShopPrefab, ItemsContainer).GetComponent<ItemUI>().Set(item);
        }
    }
}
